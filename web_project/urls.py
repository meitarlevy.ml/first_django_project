from django.contrib import admin
from django.urls import path, include

from myapp.views import CustomLoginView, questionnaire

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('login/', CustomLoginView.as_view(template_name='registration/login.html'), name='login'),
    path('', include('myapp.urls')),
    path('questionnaire/', questionnaire, name='questionnaire'),
]