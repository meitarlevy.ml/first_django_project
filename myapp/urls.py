from django.urls import path
from django.contrib.auth.views import LogoutView
from .views import RegisterView, client_answers, client_home, delete_answers, home, save_form, select_user, show_answers, submit_answers, admin_home, CustomLoginView, user_list, create_form

urlpatterns = [
    path('', CustomLoginView.as_view(template_name='registration/login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('home/', home, name='home'),
    path('client_home/', client_home, name='client_home'),
    path('admin_home/', admin_home, name='admin_home'),
    path('user_list/', user_list, name='user_list'),
    path('create_form/', create_form, name='create_form'),
    path('save_form/', save_form, name='save_form'),
    path('submit_answers/', submit_answers, name='submit_answers'),
    path('show-answers/', show_answers, name='show_answers'),
    path('show-answers/<int:user_id>/', show_answers, name='show_answers_by_user'),
    path('delete_answers/', delete_answers, name='delete_answers'),
    path('select-user/', select_user, name='select_user'),
    path('client_answers/', client_answers, name='client_answers'),
]
