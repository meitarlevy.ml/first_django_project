from django.db import models
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
    date_of_birth = models.DateField(blank=True, null=True)

    groups = models.ManyToManyField(
        'auth.Group',
        related_name='custom_user_set',
        related_query_name='user',
        blank=True,
        verbose_name='groups',
        help_text='The groups this user belongs to.',
    )
    
    user_permissions = models.ManyToManyField(
        'auth.Permission',
        related_name='custom_user_set',
        related_query_name='user',
        blank=True,
        verbose_name='user permissions',
        help_text='Specific permissions for this user.',
    )

    ROLE_CHOICES = [
        ('admin', 'Admin'),
        ('client', 'Client'),
    ]
    role = models.CharField(max_length=6, choices=ROLE_CHOICES, default='client')


    def __str__(self):
        return self.username
    
class MyAppQuestion(models.Model):
    form = models.CharField(max_length=255)
    question = models.TextField(default='Default Question Text')

    def __str__(self):
        return f'{self.form} - {self.question}'

    class Meta:
        db_table = 'myapp_question'

class Questionnaire(models.Model):
    formName = models.CharField(max_length=255)
    question1 = models.CharField(max_length=255)
    question2 = models.CharField(max_length=255)
    question3 = models.CharField(max_length=255)

class MyAppClientAnswers(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    question = models.ForeignKey(MyAppQuestion, on_delete=models.CASCADE)
    answer = models.TextField()

    def __str__(self):
        return f'{self.user.username} - {self.question.form} - {self.answer}'

    class Meta:
        db_table = 'myapp_client_answers'