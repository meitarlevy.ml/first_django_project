from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import CustomUser, MyAppClientAnswers, MyAppQuestion, Questionnaire

class MyAppClientAnswersForm(forms.ModelForm):
    class Meta:
        model = MyAppClientAnswers
        fields = ['user', 'question', 'answer']
        widgets = {
            'user': forms.HiddenInput(),
            'question': forms.HiddenInput(),
        }
        
class CustomRegistrationForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required.')
    email = forms.EmailField(max_length=254, required=True, help_text='Required. Enter a valid email address.')

    role = forms.ChoiceField(choices=CustomUser.ROLE_CHOICES)

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + ('first_name', 'last_name', 'email', 'role')
        
class MyAppQuestionForm(forms.ModelForm):
    class Meta:
        model = MyAppQuestion
        fields = ['form', 'question']

class QuestionnaireForm(forms.ModelForm):
    class Meta:
        model = Questionnaire
        fields = ['formName', 'question1', 'question2', 'question3']