from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from .forms import CustomRegistrationForm, MyAppClientAnswersForm, QuestionnaireForm
from .models import CustomUser, MyAppClientAnswers, MyAppQuestion, Questionnaire
from django.contrib import messages
from django.contrib.auth.views import LoginView
from .forms import CustomRegistrationForm, MyAppQuestionForm
from django.contrib.auth.decorators import login_required

@login_required
def show_answers(request):
    selected_user_id = request.GET.get('user_id')
    selected_user = get_object_or_404(CustomUser, id=selected_user_id) if selected_user_id else request.user

    user_answers = MyAppClientAnswers.objects.filter(user=selected_user)

    context = {'user_answers': user_answers, 'selected_user': selected_user}
    return render(request, 'show_user_answers.html', context)

@login_required
def select_user(request):
    all_users = CustomUser.objects.all()

    context = {'all_users': all_users}
    return render(request, 'select_user.html', context)

def client_home(request):
    user = request.user
    questions = MyAppQuestion.objects.all()

    user_answers = MyAppClientAnswers.objects.filter(user=user)
    user_answer_dict = {answer.question_id: answer.answer for answer in user_answers}

    if request.method == 'POST':
        if 'delete_answers' in request.POST:
            user_answers.delete()
            return HttpResponseRedirect(request.path_info)

        for question in questions:
            answer_text = request.POST.get(f'answer_{question.id}', '')
            
            if question.id in user_answer_dict:
                user_answer = user_answers.get(question_id=question.id)
                user_answer.answer = answer_text
                user_answer.save()
            else:
                MyAppClientAnswers.objects.create(user=user, question=question, answer=answer_text)

        return HttpResponseRedirect(request.path_info)

    show_update_button = bool(user_answers)

    user_answers_list = [(question, user_answer_dict.get(question.id, '')) for question in questions]

    return render(request, 'client_home.html', {'questions': questions, 'user_answers': user_answers_list, 'show_update_button': show_update_button})

def delete_answers(request):
    user = request.user
    MyAppClientAnswers.objects.filter(user=user).delete()
    return redirect('client_home')

def client_answers(request):
    user_answers = MyAppClientAnswers.objects.filter(user=request.user)
    return render(request, 'client_answers.html', {'user_answers': user_answers})

def submit_answers(request):
    if request.method == 'POST':
        messages.success(request, 'Answers submitted successfully.')
        return redirect('client_home')

    return redirect('client_home')

def questionnaire(request):
    if request.method == 'POST':
        form = QuestionnaireForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('questionnaire')
    else:
        form = QuestionnaireForm()

    questionnaires = Questionnaire.objects.all()

    return render(request, 'questionnaire.html', {'form': form, 'questionnaires': questionnaires})

class RegisterView(CreateView):
    form_class = CustomRegistrationForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        response = super().form_valid(form)
        
        self.object.first_name = form.cleaned_data['first_name']
        self.object.last_name = form.cleaned_data['last_name']
        self.object.email = form.cleaned_data['email']
        self.object.save()

        username = form.cleaned_data['username']
        password = form.cleaned_data['password1']
        user = authenticate(self.request, username=username, password=password)

        if user is not None:
            login(self.request, user)
        
        return response
    
def home(request):
    user = request.user
    if user.is_authenticated:
        if user.role == 'admin':
            return render(request, 'admin_home.html')
        else:
            return render(request, 'client_home.html')
    else:
        return redirect('login')
    
def admin_home(request):
    return render(request, 'admin_home.html')

def user_list(request):
    users = CustomUser.objects.all()
    return render(request, 'user_list.html', {'user_list': users})

def create_form(request):
    return render(request, 'create_form.html')

def save_form(request):
    if request.method == 'POST':
        form = MyAppQuestionForm(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, 'Form saved successfully.')
            return redirect('admin_home')
        else:
            messages.error(request, 'Form submission failed. Please check the entered data.')

    else:
        form = MyAppQuestionForm()

    return render(request, 'create_form.html', {'form': form})

def submit_answers(request):
    if request.method == 'POST':
        messages.success(request, 'Answers submitted successfully.')
        return redirect('client_home')

    return redirect('client_home')

class CustomLoginView(LoginView):
    success_url = reverse_lazy('home')

    def get_success_url(self):
        user = self.request.user
        if user.is_authenticated:
            if user.role == 'admin':
                return reverse_lazy('admin_home') 
            elif user.role == 'client':
                return reverse_lazy('client_home')
        return super().get_success_url()